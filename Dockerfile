FROM python:3

WORKDIR /usr/src/app

EXPOSE 8002

RUN pip install --user pipenv

# $(python -m site --user-base)/bin
ENV PATH "/root/.local/bin:${PATH}"

COPY Pipfile .
COPY Pipfile.lock .

# Install all project dependencies
RUN pipenv install