## Build the container

`$ docker build -t backendapp .`

## Run the container

```
$ docker run -it --rm \
    -v "$PWD":/usr/src/app \
    -w /usr/src/app \
    -p 8002:8002 \
    --name backendapp-running backendapp bash
```


## Start server

Inside container, run:

`# pipenv run backendapp/manage.py runserver 0.0.0.0:8002`